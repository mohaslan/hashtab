/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include <hashtab.h>

int
main(int argc, char **argv)
{
	int i;
	struct hashtab ht;
	char *key, *val;
	size_t key_size, val_size;
	size_t iter;
	char *keys[] = {"hi", "hello", "world", "hello world", "open", "source", "kernel", "hack"};
	char *values[] = {"1", "2", "3", "4", "5", "6", "7", "8"};

	if (!hashtab_init(&ht, 4, NULL))
		errx(1, "hashtab_init");
	for (i = 0; i < 8; i++) {
		hashtab_put(&ht, keys[i], strlen(keys[i]), values[i], strlen(values[i]) + 1);
		printf("%s -> %s\n", keys[i], values[i]);
	}
	printf("\n");
	for (i = 0; i < 8; i++) {
		hashtab_get(&ht, keys[i], strlen(keys[i]), (void **) &val, &val_size);
		printf("%s -> %s\n", keys[i], val);
	}

	printf("\nloop 1\n");
	if (hashtab_first(&ht, &iter)) {
		do {
			hashtab_at(&ht, iter, (void *) &key, &key_size, (void **) &val, &val_size);
			printf("%s -> %s\n", key, val);
		} while (hashtab_next(&ht, &iter));
	}
	printf("done\n");

	printf("loop 2\n");
	HASHTAB_FOREACH(ht, iter, key, key_size, val, val_size) {
		hashtab_at(&ht, iter, (void *) &key, &key_size, (void **) &val, &val_size);
		printf("%s -> %s\n", key, val);
	}
	printf("done\n");


	printf("\n");
	hashtab_put(&ht, "world", strlen("world"), "10", strlen("10") + 1);
	hashtab_get(&ht, "world", strlen("world"), (void **) &val, &val_size);
	printf("world -> %s\n", val);
	printf("\n");
	hashtab_del(&ht, "world", strlen("world"));
	if (hashtab_get(&ht, "world", strlen("world"), (void **) &val, &val_size))
		printf("world -> %s\n", val);
	else
		printf("world not found\n");
	return 0;
}
